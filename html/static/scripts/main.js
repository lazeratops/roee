var gridState;
var body = document.getElementsByTagName("body")[0];
var currentTick = 0;
var agents = [];
var totalTicks = 0;
var paused = true;
var dataSetId = "";


function loadDataSet(dsId, tickCount) {
    currentTick = 0;
    gridState = null;
    totalTicks = tickCount;
    dataSetId = dsId;
    getJson();
    dsHeading = document.getElementById("datasetHeading");
    dsHeading.innerHTML = dsId;
}


function getJson()
{
    $.getJSON("data/" + dataSetId + "/" + currentTick + ".json", function(json) {
        gridState = json; // this will show the info it in firebug console
        generateGrid(currentTick);
    });
}


function nextTick() {
    currentTick++;
    gridDiv.innerHTML = "";
    agents = [];
    getJson();
    generateGrid(currentTick);
}

function prevTick() {
    currentTick--;
    gridDiv.innerHTML = "";
    agents = [];
    getJson();
    generateGrid(currentTick);
}


function togglePause() {
    if (paused) {
        paused = false;
    } else {
        paused = true;
    }
    console.log("Paused: " + paused);
}


setInterval(onTimerTick, 33); // 33 milliseconds = ~ 30 frames per sec

function onTimerTick() {
    if (!paused && currentTick < totalTicks) {
        nextTick()
    }
}

