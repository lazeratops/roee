package lib

import (
	"io/ioutil"
	"log"
	"os"
)

func MakeAndOpenFile(filePath string) *os.File {
	var file *os.File
	_, err := os.Stat(filePath)
	if err != nil {
		_, err = os.Create(filePath)
		file, err = os.OpenFile(filePath, os.O_APPEND|os.O_RDWR, os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
	return file
}

func OpenFile(filePath string) *os.File {
	var file *os.File
	_, err := os.Stat(filePath)
	if err != nil {
		return nil
	}
	file, err = os.OpenFile(filePath, os.O_RDWR, os.ModePerm)
	if err != nil {
		panic(err)
	}
	return file
}

func ListAllFilesInDir(dirPath string) []string {
	var res []string
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		res = append(res, f.Name())
	}
	return res
}

func GetDataTickCounts(dirPath string) map[string]int {
	res := make(map[string]int)
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		if f.IsDir() {
			dirPath := dirPath + "/" + f.Name()
			subfiles, err := ioutil.ReadDir(dirPath)
			if err != nil {
				log.Fatal(err)
			}
			var ticks int
			for _, sf := range subfiles {
				if !sf.IsDir() {
					ticks++
				}
			}
			res[f.Name()] = ticks
		}
	}
	return res
}
