package agent

import (
	"fmt"
	world2 "gitlab.com/drakonka/roee/lib/abm/world"
	"sync"
)

type Thing struct {
	Id           int
	Pos          world2.Pos
	Age          int
	Instructions []world2.Instruction
	broadcaster  *world2.Broadcaster
	lock         *sync.RWMutex
}

func (a *Thing) Init(ins world2.Instruction) {
	if ins == nil {
		ins = world2.GetRandInstruction()
	}
	a.Instructions = append(a.Instructions, ins)
	a.lock = &sync.RWMutex{}
}

func (a *Thing) AddListener() *world2.Listener {
	if a.broadcaster == nil {
		a.broadcaster = &world2.Broadcaster{}
	}
	l := a.broadcaster.AddListener()
	l.Name = a.GetUniqueId()
	return l
}

func (a *Thing) Broadcast(msg string) {
	a.broadcaster.Broadcast(msg)
}

func (a *Thing) GetId() int {
	return a.Id
}

func (a *Thing) GetUniqueId() string {
	return fmt.Sprintf("%d,%d", a.Pos.X, a.Pos.Y)
}

func (a *Thing) SetId(i int) {
	a.Id = i
}

func (a *Thing) GetPos() world2.Pos {
	return a.Pos
}

func (a *Thing) SetPos(p world2.Pos) {
	a.Pos = p
}

func (a *Thing) GetInstructions() []world2.Instruction {
	return a.Instructions
}

func (a *Thing) SetInstructions(i []world2.Instruction) {
	a.Instructions = i
}

func (a *Thing) GetAge() int {
	return a.Age
}

func (a *Thing) SetAge(age int) {
	a.Age = age
}

func (a *Thing) Do() {
	a.lock.Lock()
	a.Age++
	for _, i := range a.Instructions {
		i.Do(a, world2.World)
	}
	a.lock.Unlock()
}

func (a *Thing) GetLock() *sync.RWMutex {
	if a.lock == nil {
		a.lock = &sync.RWMutex{}
	}
	return a.lock
}

func (a *Thing) Destroy() {
	fmt.Printf("\n Destroying %s", a.GetUniqueId())
	if a.broadcaster == nil {
		return
	}
	a.Broadcast("death")
}
