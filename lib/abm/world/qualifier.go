package world

type Qualifier interface {
	Init(world *Grid)
	Qualify(agents map[string]Agent) []Aggregate
	QualifyAgent(a1uid, a2uid interface{}) bool
	MergeAdjacentGroups(uid interface{})
}
