package world

import "sync"

// This is a meta model

type Aggregate interface {
	Init(w *Grid)
	GetId() int
	SetId(id int)
	GetAge() int
	GetName() string
	GetPos() Pos
	Getlock() *sync.RWMutex
	GetSize() (int, error)
	AddElement(e interface{})
	RemoveElement(e interface{})
	GetElements() map[string]*Listener // Calling this "elements" because these do NOT Have to be Agents.
	SetElements(elements []interface{})
	ElementKeys() []interface{}
	GetQualifiers() []Qualifier
	SetQualifiers(q []Qualifier)
	Getworld() *Grid
	Qualify(uid interface{}) bool
	Destroy()
}
