package world

import (
	"math/rand"
	"reflect"
)

type Instruction interface {
	Init()
	GetName() string
	Do(params ...interface{})
}

func GetRandInstruction() Instruction {
	it := getRandomInstructionType()
	ins := reflect.New(it).Interface().(Instruction)
	return ins
}

type direction int

const (
	up    direction = 0
	down  direction = 1
	left  direction = 2
	right direction = 3
)

var possibleInstructions = []reflect.Type{
	reflect.TypeOf(Copy{}),
	reflect.TypeOf(Del{}),
}

type Copy struct {
	Name string
}

func (i *Copy) Init() {
	i.Name = "copy"
}

func (i *Copy) GetName() string {
	return i.Name
}

func (i *Copy) Do(params ...interface{}) {
	agent := params[0].(Agent)
	world := params[1].(*Grid)
	cPos := agent.GetPos()
	direction := direction(rand.Intn(5))

	newAgent := reflect.New(reflect.ValueOf(agent).Elem().Type()).Interface().(Agent)
	newAgent.SetId(agent.GetId())
	switch direction {
	case up:
		newAgent.SetPos(Pos{X: cPos.X, Y: cPos.Y - 1})
	case down:
		newAgent.SetPos(Pos{X: cPos.X, Y: cPos.Y + 1})
	case left:
		newAgent.SetPos(Pos{X: cPos.X - 1, Y: cPos.Y})
	case right:
		newAgent.SetPos(Pos{X: cPos.X + 1, Y: cPos.Y})
	}

	newAgent.Init(GetRandInstruction())
	world.AddAgent(newAgent)
}

type Del struct {
	Name string
}

func (i *Del) Init() {
	i.Name = "del"
}

func (i *Del) GetName() string {
	return i.Name
}

func (i *Del) Do(params ...interface{}) {
	agent := params[0].(Agent)
	world := params[1].(*Grid)
	cPos := agent.GetPos()
	direction := direction(rand.Intn(5))

	var deletionPos Pos
	switch direction {
	case up:
		deletionPos = Pos{X: cPos.X, Y: cPos.Y - 1}
	case down:
		deletionPos = Pos{X: cPos.X, Y: cPos.Y + 1}
	case left:
		deletionPos = Pos{X: cPos.X - 1, Y: cPos.Y}
	case right:
		deletionPos = Pos{X: cPos.X + 1, Y: cPos.Y}
	}
	world.DeleteAgent(deletionPos)
}
