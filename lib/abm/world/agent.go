package world

import "sync"

type Agent interface {
	Init(int Instruction)
	GetId() int
	GetUniqueId() string
	SetId(i int)
	GetPos() Pos
	SetPos(p Pos)
	GetLock() *sync.RWMutex
	GetInstructions() []Instruction
	SetInstructions([]Instruction)
	GetAge() int
	SetAge(int)
	AddListener() *Listener
	Broadcast(msg string)
	Do()
	Destroy()
}

type Pos struct {
	X, Y int
}
