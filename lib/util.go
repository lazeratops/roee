package lib

import (
	"fmt"
	"time"
)

func TimeElapsed(label string, minDuration time.Duration) func() {
	st := time.Now()
	return func() {
		el := time.Since(st)
		if el >= minDuration {
			fmt.Printf("%s ran in %v\n", label, el)
		}
	}
}

func UnionInterface(target []interface{}, newElements []interface{}) []interface{} {
	for _, newE := range newElements {
		var isDupe bool
		for _, e := range target {
			if e == newE {
				isDupe = true
				break
			}
		}
		if isDupe == false {
			target = append(target, newE)
		}
	}
	return target
}

func UnionStr(target []string, newElements []string) []string {
	for _, newE := range newElements {
		var isDupe bool
		for _, e := range target {
			if e == newE {
				isDupe = true
				break
			}
		}
		if isDupe == false {
			target = append(target, newE)
		}
	}
	return target
}
