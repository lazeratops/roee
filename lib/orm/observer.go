package orm

import (
	"gitlab.com/drakonka/roee/lib/abm/world"
	"sync"
)

type Observer interface {
	Init(wg *sync.WaitGroup)
	Watch(grid world.Grid)
	GridChan() chan world.Grid
	NextStep() Reifier
}
