package orm

import (
	"bufio"
	"fmt"
	"gitlab.com/drakonka/roee/lib"
	"os"
	"reflect"
	"strings"
	"sync"
)

type Modifier interface {
	Init()
	AddReifiedEntity(re reifiedEntity)
	Do(wg *sync.WaitGroup) error
}

var typeRegistry = make(map[string]reflect.Type)

func makeInstance(name string) interface{} {
	v := reflect.New(typeRegistry[name]).Elem()
	// Maybe fill in fields here if necessary
	return v.Interface()
}

func (m *aggregationModifier) implementInterface(name, pkgName string, itype reflect.Type) string {
	var lines []string
	// Opening tag
	l1 := fmt.Sprintf("package %s", pkgName)
	l2 := fmt.Sprintf("type %s struct {}", name)

	lines = []string{l1}
	imports, s := implementMethods(itype, name)
	if len(imports) > 0 {
		lines = append(lines, "import (\n")
		lines = append(lines, imports)
		lines = append(lines, ")\n")
	}
	lines = append(lines, l2)
	lines = append(lines, s)

	return strings.Join(lines, "\n")
}

func implementMethods(itype reflect.Type, name string) (string, string) {
	var imports, inparams, outparams string
	var s string
	// Find what we need to implement
	for i := 0; i < itype.NumMethod(); i++ {
		var ret, body string
		m := itype.Method(i)
		inparams, imports = processInParams(m, imports)
		outparams, imports, ret, body = processOutParams(m, imports)

		s += fmt.Sprintf(`

	func (a *%s) %s(%v) %v {
		%s
		%s
	}

`, name, m.Name, inparams, outparams, body, ret)

	}
	return imports, s
}

func implementMethod(f *os.File, name, body string) {
	// We will overwrite existing method if it already exists
	// Otherwise add a new method
	var newContents string
	var rewroteExisting bool
	f.Seek(0, 0)
	s := bufio.NewScanner(f)
	var nestingLevel int

	for s.Scan() {
		l := s.Text()
		if err := s.Err(); err != nil {
			fmt.Println(err)
		}
		trimmedL := strings.TrimSpace(l)
		fName := getFuncName(trimmedL)
		if fName == name {
			newContents += "\n" + body
			rewroteExisting = true
			nestingLevel = updateNestingLevel(trimmedL, nestingLevel)
		} else {
			if nestingLevel > 0 {
				nestingLevel = updateNestingLevel(trimmedL, nestingLevel)
			} else {
				newContents += "\n" + l
			}
		}
	}
	if !rewroteExisting {
		newContents += "\n" + body
	}

	f.Truncate(0)
	f.Seek(0, 0)
	f.WriteString(newContents)
	f.Sync()
}

func processOutParams(m reflect.Method, imports string) (string, string, string, string) {
	var out string
	ret := "return "
	var body string
	numOut := m.Type.NumOut()
	if numOut > 0 {
		out += "("
		for idx := 0; idx < numOut; idx++ {
			var retparams string
			var outparams string
			imports, outparams, body, retparams = processOutParam(m, idx, imports, body, retparams)
			if idx < numOut-1 {
				retparams += ","
				outparams += ","
			}
			ret += retparams
			out += outparams
		}
		out += ")"
	}
	return out, imports, ret, body
}

func processOutParam(m reflect.Method, idx int, imports string, body string, retparams string) (string, string, string, string) {
	p := m.Type.Out(idx)
	pTypeName := p.String()
	pkgPath := p.PkgPath()
	if len(pkgPath) > 0 {
		if strings.Contains(imports, pkgPath) == false {
			imports += fmt.Sprintf("\"%s\" \n", pkgPath)
		}
	}
	out := fmt.Sprintf("%s", pTypeName)
	// We need to initialize this..
	body += fmt.Sprintf("\n		var out%d %s", idx, pTypeName)

	retparams += fmt.Sprintf("out%d", idx)
	return imports, out, body, retparams
}

func processInParams(m reflect.Method, imports string) (string, string) {
	var inparams string
	for idx := 0; idx < m.Type.NumIn(); idx++ {
		imports, inparams = processInParam(m, idx, imports, inparams)
	}
	return inparams, imports
}

func processInParam(m reflect.Method, idx int, imports string, inparams string) (string, string) {
	p := m.Type.In(idx)
	pkgPath := p.PkgPath()
	if len(pkgPath) > 0 {
		if strings.Contains(imports, pkgPath) == false {
			imports += fmt.Sprintf("\"%s\" \n", pkgPath)
		}
	}
	inparams += fmt.Sprintf("param%d %s", idx, p.String())
	if idx < m.Type.NumIn()-1 {
		inparams += ","
	}
	return imports, inparams
}

type part struct {
	contents string
}

func implementFields(f *os.File) error {
	var newContents []*part
	newContents = append(newContents, &part{})

	s := bufio.NewScanner(f)
	preFieldDefs := &part{}
	fieldDefs := &part{contents: "{"}
	postFieldDefs := &part{}
	newContents = append(newContents, preFieldDefs)
	newContents = append(newContents, fieldDefs)
	newContents = append(newContents, postFieldDefs)
	var foundStructDefStart bool
	var structDefPrepLvl int
	var offsetToWriteFields int64
	var replaceNextReturnWith string
	for s.Scan() {

		l := s.Text()
		if err := s.Err(); err != nil {
			fmt.Println(err)
		}
		if foundStructDefStart == false {
			if getStructName(l) == "" {
				offsetToWriteFields += int64(len(s.Bytes()))
			} else {
				foundStructDefStart = true
			}
		}

		if foundStructDefStart && structDefPrepLvl < 2 {
			var newL string
			for _, c := range l {
				s := string(c)
				if s == "{" || s == "}" {
					structDefPrepLvl++
				} else {
					newL += s
				}
			}
			preFieldDefs.contents += newL
			continue
		}

		if !foundStructDefStart {
			preFieldDefs.contents += "\n" + l
		} else {
			var trimmed = strings.TrimSpace(l)

			if strings.HasPrefix(trimmed, "var out") && len(replaceNextReturnWith) > 0 {
				// Skip this completely, we don't want to include autogenerated out vars when overwriting the return
			} else if strings.HasPrefix(trimmed, "return") && len(replaceNextReturnWith) > 0 {
				postFieldDefs.contents += "\n return " + replaceNextReturnWith
				replaceNextReturnWith = ""
			} else {
				postFieldDefs.contents += "\n" + l
			}
		}

		funcName, retTypes := getFuncNameAndRetType(l)

		// If func doesn't expect any returns OR more than one return, it is not a "getter func" that needs a struct field.
		if funcName != "" {
			if len(retTypes) == 1 {
				fieldName := getGetterName(funcName)
				if fieldName != "" {
					fieldDef := fmt.Sprintf("\n\t%s %s", fieldName, retTypes[0])
					fieldDefs.contents += fieldDef
					replaceNextReturnWith = "a." + fieldName
				}
			} else if len(retTypes) == 0 {
				fieldName := getSetterName(funcName)
				if fieldName != "" {
					// Try to find input params
					inParamNames, _ := getInputParams(l)
					line := fmt.Sprintf("\na.%s = %s", fieldName, inParamNames[0])
					postFieldDefs.contents += line
				}
			}
		}

	}

	f.Truncate(0)
	f.Seek(0, 0)
	fieldDefs.contents += "\n}"
	for _, p := range newContents {
		f.WriteString(p.contents)
	}
	f.Sync()
	return nil
}

// Assuming we have one param
func getInputParams(line string) (names []string, types []string) {
	trimmed := strings.TrimSpace(line)
	parts := strings.Split(trimmed, "(")
	if len(parts) == 3 {
		part := parts[2]
		part = strings.Replace(part, ")", "", -1)
		part = strings.Replace(part, "{", "", -1)
		part = strings.Replace(part, "}", "", -1)
		part = strings.TrimSpace(part)
		inParams := strings.Split(part, ",")
		for _, param := range inParams {
			spaceSplit := strings.Split(param, " ")
			names = append(names, spaceSplit[0])
			types = append(types, spaceSplit[1])

		}

	}
	return names, types
}

func replaceReturn(s string, new string) string {
	lines := strings.Split(s, "\n")
	for _, l := range lines {
		trimmed := strings.TrimSpace(l)
		if strings.HasPrefix(trimmed, "return") {
			newLine := "return " + new
			return newLine
		}
	}
	return ""
}

func getStructName(line string) string {
	trimmed := strings.TrimSpace(line)
	structIdx := strings.LastIndex(trimmed, "struct")
	if !strings.HasPrefix(trimmed, "type") || structIdx == -1 {
		return "" // Not a struct definition
	}
	return trimmed[3:structIdx]
}

func getGetterName(funcName string) string {
	lFuncName := strings.ToLower(funcName)
	if strings.HasPrefix(lFuncName, "get") == false {
		return "" // Not a getter func
	}
	return funcName[3:]
}

func getSetterName(funcName string) string {
	lFuncName := strings.ToLower(funcName)
	if strings.HasPrefix(lFuncName, "set") == false {
		return "" // Not a getter func
	}
	return funcName[3:]
}

/* func findFuncBody(f *os.File, wantedFuncName string) (string, error) {
	s := bufio.NewScanner(f)
	var body string
	var foundFuncStart bool
	var nestingLevel int
	for s.Scan() {
		l := s.Text()
		if err := s.Err(); err != nil {
			fmt.Println(err)
		}
		if !foundFuncStart {
			funcName := getFuncName(l)
			if funcName == wantedFuncName {
				foundFuncStart = true
				nestingLevel = 1
				continue
			}
		}
		if foundFuncStart {
			nestingLevel = updateNestingLevel(l, nestingLevel)
			if nestingLevel > 0 {
				body += l
			}
		}
	}
	return body, nil
} */

func updateNestingLevel(line string, nestingLevel int) int {
	// Count # of { and } chars in the line
	numOpen := strings.Count(line, "{")
	numClose := strings.Count(line, "}")
	if numOpen == numClose {
		return nestingLevel
	}
	diff := numOpen - numClose
	nestingLevel += diff
	return nestingLevel
}

func getFuncName(line string) string {
	trimmed := strings.TrimSpace(line)
	if strings.HasPrefix(trimmed, "func") == false {
		return "" // Not a function
	}

	var name string

	trimmedPastFunc := strings.TrimSpace(trimmed[4:])
	inReceiver := false
	for _, c := range trimmedPastFunc {
		s := string(c)
		if s == "(" && len(name) == 0 {
			// Clearly this is the start of a receiver, in this case look for closing brace
			inReceiver = true
			continue
		}
		if inReceiver {
			if s == ")" {
				inReceiver = false
			}
			continue
		}
		if s == "(" {
			// We're past the name now
			return strings.TrimSpace(name)
		}
		name += s
	}

	// We should never get here.
	return name
}

func getFuncNameAndRetType(line string) (string, []string) {
	var trimmed string
	for _, c := range strings.TrimSpace(line) {
		s := string(c)
		if s != " " && s != "\n" {
			trimmed += s
		}
	}

	// Early out if this line doesn't even containa func
	if strings.HasPrefix(trimmed, "func") == false {
		return "", nil // Not a function
	}

	var funcName string
	var retTypes []string

	var nameDone bool
	var inParamsDone bool
	// Trim everything from "func" on
	trimmedPastFunc := strings.TrimSpace(trimmed[4:])

	inReceiver := false
	inInParams := false
	inRetTypes := false

	var retType string

	// First () will be receiver; Second () will be func params; Third () will be return types.
	for _, c := range trimmedPastFunc {
		s := string(c)
		if s == "(" && len(funcName) == 0 {
			// Clearly this is the start of a receiver, in this case look for closing brace
			inReceiver = true
			continue
		}
		if inReceiver {
			if s == ")" {
				inReceiver = false
			}
			continue
		}

		if !inParamsDone {
			if !inInParams && s == "(" {
				nameDone = true
				inInParams = true
				continue
			}
			if inInParams && s == ")" {
				inInParams = false
				inParamsDone = true
				continue
			}
		}

		// NOW we must be in the return types..
		if inParamsDone && s == "(" {
			inRetTypes = true
			continue
		}
		if inRetTypes == true {
			if s == ")" {
				retTypes = append(retTypes, strings.TrimSpace(retType))
				return strings.TrimSpace(funcName), retTypes
			} else if s == "," {
				retTypes = append(retTypes, strings.TrimSpace(retType))
				retType = ""
			} else {
				retType += s
			}
		}
		if !nameDone {
			funcName += s
		}
	}

	// We should never get here.
	return funcName, retTypes
}

func addToSlice(f *os.File, sliceName string, newElements []string) {
	f.Seek(0, 0)
	s := bufio.NewScanner(f)

	var nestingLevel int
	var newContents []*part
	var varDefinition = &part{}
	newContents = append(newContents, &part{})
	newContents = append(newContents, varDefinition)

	var foundVar bool
	for s.Scan() {
		l := s.Text()
		if err := s.Err(); err != nil {
			fmt.Println(err)
		}

		if !foundVar {
			varName := getVarName(l)
			if varName == sliceName {
				foundVar = true
			}
		}

		if foundVar && len(newContents) < 3 {
			nestingLevel = updateNestingLevel(l, nestingLevel)
			if nestingLevel > 0 {
				varDefinition.contents += fmt.Sprintf("\n%s", l)
				continue
			}
			for _, e := range newElements {
				varDefinition.contents += fmt.Sprintf("%s,", e)
			}
			newContents = append(newContents, &part{})
		}

		if !foundVar {
			newContents[0].contents += fmt.Sprintf("\n%s", l)
		} else if foundVar && len(newContents) == 3 {
			newContents[2].contents += fmt.Sprintf("\n%s", l)
		}
	}

	f.Truncate(0)
	f.Seek(0, 0)
	for _, p := range newContents {
		f.WriteString(p.contents)
	}
	f.Sync()

}

func addImports(f *os.File, newImports []string) {
	f.Seek(0, 0)
	s := bufio.NewScanner(f)

	var newContents []*part
	newContents = append(newContents, &part{})

	var imports = &part{}
	// Imports will be inside part 1
	newContents = append(newContents, imports)

	var inImports bool
	var existingImports []string

	for s.Scan() {
		l := s.Text()
		if err := s.Err(); err != nil {
			fmt.Println(err)
		}

		// If we haven't even gotten to the imports yet...
		if len(existingImports) == 0 && !inImports {
			// Pre-import content will be in part 0
			newContents[0].contents += fmt.Sprintf("\n%s", l)
			trimmedLine := strings.TrimSpace(l)
			parts := strings.Split(trimmedLine, " ")
			if len(parts) == 2 && parts[0] == "import" && parts[1] == "(" {
				// Need to be sure to get rid of or avoid any duplicate imports here.
				inImports = true
				newContents = append(newContents, &part{})
			}
		} else if inImports {
			// We have now found the imports and MAY be in them, let's check
			if inImports {
				// The block of imports ends with a ")"
				trimmedLine := strings.TrimSpace(l)
				if trimmedLine == ")" {
					inImports = false
					newContents[2].contents += fmt.Sprintf("\n%s", l)
				} else {
					existingImports = append(existingImports, l)
				}
			}
		} else {
			// Post-import content will be in part 2
			newContents[2].contents += fmt.Sprintf("\n%s", l)
		}

	}
	for i, ni := range newImports {
		newImports[i] = fmt.Sprintf("\"%s\"", ni)
	}
	existingImports = lib.UnionStr(existingImports, newImports)
	for _, imp := range existingImports {
		imports.contents += fmt.Sprintf("\n%s", imp)
	}

	f.Truncate(0)
	f.Seek(0, 0)
	for _, p := range newContents {
		f.WriteString(p.contents)
	}
	f.Sync()
}

func getVarName(line string) string {
	trimmed := strings.TrimSpace(line)
	splitLine := strings.Split(trimmed, " ")
	if strings.HasPrefix(trimmed, "var ") == true {
		return splitLine[1] // this is definitely a var
	}

	for idx, word := range splitLine {
		if word == ":=" {
			return splitLine[idx-1]
		}
	}
	// This must not be a var at all
	return ""
}
