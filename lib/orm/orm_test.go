package orm

import (
	"fmt"
	"gitlab.com/drakonka/roee/lib/abm"
	"testing"
)

func TestGetDistance(t *testing.T) {
	p1 := abm.Pos{
		X: 0,
		Y: 0,
	}
	p2 := abm.Pos{
		X: 7,
		Y: 9,
	}
	o := AggregationObserver{}
	a := o.getDistance(p1, p2)
	fmt.Printf("Distance: %v", a)
}

func TestGetAllGridDistances(t *testing.T) {
	g := &abm.Grid{}
	g.X = 10
	g.Y = 10
	g.Population = make(map[string]abm.Agent)
	g.Population["0,0"] = &abm.Thing{Id: 1, Pos: abm.Pos{X: 0, Y: 0}}
	g.Population["1,1"] = &abm.Thing{Id: 2, Pos: abm.Pos{X: 1, Y: 1}}
	g.Population["0,2"] = &abm.Thing{Id: 3, Pos: abm.Pos{X: 0, Y: 2}}
	g.Population["3,1"] = &abm.Thing{Id: 4, Pos: abm.Pos{X: 3, Y: 1}}
	g.Population["7,4"] = &abm.Thing{Id: 5, Pos: abm.Pos{X: 7, Y: 4}}
	g.Population["6,8"] = &abm.Thing{Id: 6, Pos: abm.Pos{X: 6, Y: 8}}
	g.Population["7,9"] = &abm.Thing{Id: 7, Pos: abm.Pos{X: 7, Y: 9}}

	o := AggregationObserver{}
	o.watch()
	// fmt.Println(len(proximities))
}
