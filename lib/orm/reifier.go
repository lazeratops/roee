package orm

import (
	"gitlab.com/drakonka/roee/lib/abm/world"
	"sync"
)

// What does the reifier need to go by? What will it produce?
// Input: Property name, min distance, max distance
// Output: custom made class
type Reifier interface {
	Init()
	AddParam(p interface{})
	Do(wg *sync.WaitGroup, params ...interface{}) error
	NextStep() Modifier
}

type reifiedEntity interface {
	Name() string
	Qualifiers() []world.Qualifier
}
