package orm

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/roee/lib/abm/aggregate"
	"gitlab.com/drakonka/roee/lib/abm/world"
	"math/rand"
	"sync"
	"time"
)

type aggregationReifier struct {
	modifier         Modifier
	reifiedAggregate reifiedAggregate
	Params           []interface{}
}

func (r *aggregationReifier) Init() {
	r.modifier = &aggregationModifier{}
	r.modifier.Init()
}

func (r *aggregationReifier) AddParam(p interface{}) {
	r.Params = append(r.Params, p)
}

func (r *aggregationReifier) NextStep() Modifier {
	return r.modifier
}

// Currently this takes only one observation and reifies it..
// what if down the line we want to feed it multiple observations?
// Or rather one observation with multiple qualifiers
func (r *aggregationReifier) Do(wg *sync.WaitGroup, params ...interface{}) error {
	for _, p := range r.Params {
		newQualifier := p.(aggregate.AggregateQualifier)
		// Check if an aggregate with this kind of qualifier already exists.
		for _, q := range world.Qualifiers {
			aq, ok := q.(*aggregate.AggregateQualifier)
			if ok {
				if aq.FieldName == newQualifier.FieldName &&
					aq.MinDist == newQualifier.MinDist &&
					aq.MaxDist == newQualifier.MaxDist &&
					aq.MinQuant == newQualifier.MinQuant &&
					aq.MaxQuant == newQualifier.MaxQuant {
					wg.Done()
					msg := fmt.Sprintf("This qualifier already exists under the name of %s", aq.TypeToInstantiate.Name())
					return errors.New(msg)
				}
			}
		}

		n := generateName(newQualifier.FieldName)
		e := reifiedAggregate{name: n, qualifiers: []aggregate.AggregateQualifier{newQualifier}}
		r.reifiedAggregate = e
		r.modifier.AddReifiedEntity(&e)
	}
	// Now we need to send the aggregate to a modifier to actually modify the code...
	wg.Done()
	return nil
}

func generateName(fn string) string {
	rand.Seed(time.Now().UnixNano())
	rint := rand.Intn(10000)
	aggregateName := fmt.Sprintf("Aggregate%s%d", fn, rint)
	return aggregateName
}

type reifiedAggregate struct {
	name       string
	qualifiers []aggregate.AggregateQualifier
}

func (ra *reifiedAggregate) Name() string {
	return ra.name
}

func (ra *reifiedAggregate) Qualifiers() []world.Qualifier {
	var qq []world.Qualifier
	for _, q := range ra.qualifiers {
		qq = append(qq, &q)
	}
	return qq
}
