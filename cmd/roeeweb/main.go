package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"runtime"
	"strings"
	"html/template"
	"gitlab.com/drakonka/roee/lib"
)

func main() {
	r := newRouter()

	http.Handle("/", r)
	http.ListenAndServe(":8081", nil)

}

func newRouter() *mux.Router {
	r := mux.NewRouter()
	r.StrictSlash(true)

	r.PathPrefix("/static/").Handler(
		http.StripPrefix("/static", http.FileServer(http.Dir(getProjectRootPath() + "/html/static/"))),
	).Methods("GET")

	r.PathPrefix("/data/").Handler(
		http.StripPrefix("/data", http.FileServer(http.Dir(getProjectRootPath() + "/lib/orm/data/"))),
	).Methods("GET")

	tmpl := template.Must(template.ParseFiles(getProjectRootPath() + "/html/index.html"))

	files := lib.GetDataTickCounts(getProjectRootPath() + "/lib/orm/data")
	r.HandleFunc("/", func(rw http.ResponseWriter, req *http.Request) {
		varmap := map[string]interface{}{
			"datasets": files,
		}
		tmpl.ExecuteTemplate(rw, "index", varmap)
	})

	http.Handle("/", r)
	http.ListenAndServe(":8081", nil)
	return r
}


func getProjectRootPath() string {
	_, b, _, _ := runtime.Caller(0)
	folders := strings.Split(b, "/")
	folders = folders[:len(folders)-3]
	path := strings.Join(folders, "/")
	return path
}
